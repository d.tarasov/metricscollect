package com.example.metricsCollect;

import com.example.metricsCollect.entity.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.util.*;

@org.springframework.stereotype.Controller
@RequestMapping(path = "/api/v1/metrics")
public class Controller {
    @Autowired
    private Repository repository;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @PostMapping(path = "/post", consumes = "application/json")
    public Iterable<Attribute> addNewMetrics(@RequestBody List<Attribute> attribute) {
        for (Attribute a : attribute) {
            a.setOperationTimestamp(OffsetDateTime.now());
        }
        return repository.saveAll(attribute);
    }

    @GetMapping(path = "/get/all/{metric_name}")
    public @ResponseBody
    Iterable<Attribute> getAllMetrics(@PathVariable String metric_name) {
        return repository.findTop3ByNameOrderByTimestampDesc(metric_name);
    }

    @GetMapping(path = "/get/all/{metric_name}/{count}")
    public @ResponseBody
    Iterable<Attribute> getAllMetrics(@PathVariable String metric_name, @PathVariable int count) {
        log.info(String.valueOf(count));
        return repository.findAllByName(metric_name, PageRequest.of(0, count, Sort.by(Sort.Direction.DESC,"timestamp")));
    }
}
