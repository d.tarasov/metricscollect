package com.example.metricsCollect.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class Attribute {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(length = 64)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    private Double value;

    @Column(length = 1024)
    private String params;

    private OffsetDateTime timestamp;

    private OffsetDateTime requestTimestamp;

    private OffsetDateTime operationTimestamp;

    public Attribute() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() { return status; }

    public void setStatus(Status status) { this.status = status; }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public OffsetDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(OffsetDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public OffsetDateTime getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(OffsetDateTime requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public OffsetDateTime getOperationTimestamp() {
        return operationTimestamp;
    }

    public void setOperationTimestamp(OffsetDateTime operationTimestamp) {
        this.operationTimestamp = operationTimestamp;
    }

}

enum Status {
    UP, DOWN, OVERLOADED, NOT_RESPONDING
}
