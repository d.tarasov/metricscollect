package com.example.metricsCollect;

import com.example.metricsCollect.entity.Attribute;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Repository extends CrudRepository<Attribute, Long> {
    List<Attribute> findTop3ByNameOrderByTimestampDesc(String name);
    List<Attribute> findAllByName(String name, Pageable pageable);
}
