package com.example.metricsCollect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetricsCollectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetricsCollectApplication.class, args);
	}
}